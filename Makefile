# Copyright (C) 2023-2024  Umorpha Systems
# SPDX-License-Identifier: AGPL-3.0-or-later

all: bin/gen-zimage
all: bin/objcopy
.PHONY: all

.PHONY: FORCE

bin/%: cmd/%/ FORCE
	go build -trimpath -race -o $@ ./$<

bin/gen-zimage: cmd/gen-zimage/image.txt

generate/files  = COPYING.txt
generate/files += cmd/gen-zimage/image.txt
COPYING.txt:
	curl https://www.gnu.org/licenses/agpl-3.0.txt >$@
cmd/gen-zimage/image.txt:
	curl -L https://www.gutenberg.org/ebooks/84.txt.utf-8 >$@

generate: $(generate/files)
generate-clean:
	rm -f -- $(generate/files)
.PHONY: generate generate-clean
