<!--
Copyright (C) 2024  Umorpha Systems
SPDX-License-Identifier: AGPL-3.0-or-later
-->

# gen-zimage

## SYNOPSIS

Populate `./out/` with ARM zImage files across a variety of Linux
kernel versions and configurations:

	make && ./bin/gen-zimage /path/to/existing/linux-git-checkout

Like `arm-linux-gnueabi-objcopy -O binary -R .comment -S INFILE OUTFILE`:

	make && ./bin/objcopy [-scramble] INFILE OUTFILE

## DESCRIPTION

I
[needed](https://gitlab.archlinux.org/archlinux/mkinitcpio/mkinitcpio/-/merge_requests/328)
to generate ARM zImage files as test-cases, and I thought "go big or
go home", so I wrote this script to build a file for every possible
kernel configuration across every kernel version.

Instead of including a real kernel as the payload in the zImage files,
it uses `./cmd/gen-zimage/image.txt`.

I probably over-split this into packages, but oh well:

 - The `./lib/matrix` package describes the matrix of kernel
   configurations.
 - The `./lib/build` package switches git versions, patches the kernel
   build system to allow us to inject `image.txt`, applies the
   configuration, actually builds the zImage.
 - The `./lib/objcopy` package is a custom implementation of
   `arm-linux-gnueabi-objcopy -O binary -R .comment -S INFILE OUTFILE`
   that can overwrite machine code with garbage, so that the generated
   files aren't affected by the GPL for the self-extraction code in
   them.
 - The `./cmd/objcopy` command is a standalone wrapper around the
   `./lib/objcopy` library, for debugging.
 - The `./cmd/gen-zimage` command is the main program.
