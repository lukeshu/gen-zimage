// Copyright (C) 2024  Umorpha Systems
// SPDX-License-Identifier: AGPL-3.0-or-later

package objcopy

import (
	"bytes"
	"debug/elf"
	"fmt"
	"io"
	"os"
)

// ObjCopy mimics
//
//	arm-linux-gnueabi-objcopy -O binary -R .comment -S ${inFilename} ${outFilename}
//
// If [scramble], then it overwrites all the machine code with
// garbage, to avoid license violations, while retaining the
// size/layout/GOT of the file.
func ObjCopy(inFilename, outFilename string, scramble bool, stderr io.Writer) (err error) {
	maybeSetErr := func(_err error) {
		if err == nil && _err != nil {
			err = _err
		}
	}

	inFile, err := elf.Open(inFilename)
	if err != nil {
		return err
	}
	defer func() { maybeSetErr(inFile.Close()) }()

	outFile, err := os.OpenFile(outFilename, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0o666)
	if err != nil {
		return err
	}
	defer func() { maybeSetErr(outFile.Close()) }()

	firstAddr := int64(-1)
	piggySizeAddr := int64(-1)
nextSection:
	for _, section := range inFile.Sections {
		if stderr != nil {
			fmt.Fprintf(stderr, "section: %#v\n", section.SectionHeader)
		}

		// `-O binary`
		if section.SectionHeader.Type == elf.SHT_NULL ||
			section.SectionHeader.Type == elf.SHT_NOBITS ||
			section.SectionHeader.Size == 0 ||
			section.SectionHeader.Flags&elf.SHF_ALLOC == 0 {
			continue nextSection
		}

		// `-R .comment`
		if section.SectionHeader.Name == ".comment" {
			continue nextSection
		}

		// `-S`
		// Nothing to do?

		if firstAddr < 0 {
			firstAddr = int64(section.SectionHeader.Addr)
		}
		outAddr := int64(section.SectionHeader.Addr) - firstAddr
		if _, err := outFile.Seek(outAddr, io.SeekStart); err != nil {
			return err
		}
		nBytes, err := io.Copy(outFile, &statefulReader{ReaderAt: section})
		if err != nil {
			return err
		}
		if stderr != nil {
			fmt.Fprintf(stderr, "  copied: %08x(+%04x): %q\n", outAddr, nBytes, section.SectionHeader.Name)
		}

		if scramble {
			switch section.SectionHeader.Name {
			case ".table":
				continue nextSection
			case ".piggydata":
				piggySizeAddr = outAddr + int64(nBytes) - 4
				continue nextSection
			default:
				if piggySizeAddr > 0 && outAddr > piggySizeAddr {
					continue nextSection
				}
				var bufRead, bufZero [4]byte
				bufWrite := [4]byte{0xde, 0xad, 0xbe, 0xef}
				for addr := outAddr; addr < outAddr+int64(nBytes); addr += 4 {
					if 0x24 <= addr && addr <= 0x38 {
						continue
					}
					n, err := outFile.ReadAt(bufRead[:], addr)
					if err != nil && !(n > 0 && err == io.EOF) {
						return fmt.Errorf("scramble: read: %#x: %w", addr, err)
					}
					if bytes.Equal(bufRead[:n], bufZero[:n]) {
						continue
					}
					if _, err := outFile.WriteAt(bufWrite[:n], addr); err != nil {
						return fmt.Errorf("scramble: write: %w", err)
					}
				}
			}
		}
	}

	return nil
}

type statefulReader struct {
	io.ReaderAt
	pos int64
}

func (r *statefulReader) Read(dat []byte) (n int, err error) {
	n, err = r.ReadAt(dat, r.pos)
	r.pos += int64(n)
	return n, err
}
