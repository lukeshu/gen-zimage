// Copyright (C) 2024  Umorpha Systems
// SPDX-License-Identifier: AGPL-3.0-or-later

package matrix

import (
	"maps"
	"sort"
	"strings"
)

type (
	KconfigSyms map[string]KconfigVals
	KconfigVals map[string]KconfigSyms
)

func (symSpec KconfigSyms) Iter(cfg map[string]string, yield func(map[string]string) error) error {
	syms := make([]string, 0, len(symSpec))
	for sym := range symSpec {
		syms = append(syms, sym)
	}
	sort.Strings(syms)

	var subYield func(i int) func(map[string]string) error
	subYield = func(i int) func(map[string]string) error {
		if i >= len(syms) {
			return yield
		}
		return func(cfg map[string]string) error {
			sym := syms[i]
			valSpec := symSpec[sym]
			vals := make([]string, 0, len(valSpec))
			for val := range valSpec {
				vals = append(vals, val)
			}
			sort.Strings(vals)

			for _, val := range vals {
				subCfg := maps.Clone(cfg)
				if !strings.HasSuffix(sym, "_") {
					// normal
					subCfg[sym] = val
				} else {
					// choice
					for _, oVal := range vals {
						if oVal == "" {
							continue
						}
						subCfg[sym+oVal] = ""
					}
					if val != "" {
						subCfg[sym+val] = "y"
					}
				}
				if err := valSpec[val].Iter(subCfg, subYield(i+1)); err != nil {
					return err
				}
			}
			return nil
		}
	}

	return subYield(0)(cfg)
}

func (o KconfigSyms) deepClone() KconfigSyms {
	if o == nil {
		return nil
	}
	ret := make(KconfigSyms, len(o))
	for k, v := range o {
		ret[k] = v.deepClone()
	}
	return ret
}

func (o KconfigVals) deepClone() KconfigVals {
	if o == nil {
		return nil
	}
	ret := make(KconfigVals, len(o))
	for k, v := range o {
		ret[k] = v.deepClone()
	}
	return ret
}

func Merge(ins ...KconfigSyms) KconfigSyms {
	out := make(KconfigSyms)
	for _, in := range ins {
		for sym, newVals := range in {
			oldVals := out[sym]
			if oldVals == nil {
				out[sym] = newVals.deepClone()
			} else {
				for val := range newVals {
					oldVals[val] = Merge(oldVals[val], newVals[val])
				}
			}
		}
	}
	if len(out) == 0 {
		return nil
	}
	return out
}

var (
	boolVal = KconfigVals{
		"":  nil,
		"y": nil,
	}

	appendedDTBSyms = KconfigSyms{
		"CONFIG_ARM_APPENDED_DTB": {
			"": nil,
			"y": {
				"CONFIG_ARM_ATAG_DTB_COMPAT": { // mentioned in arch/arm/boot/compressed/
					"": nil,
					"y": {
						"CONFIG_ARM_ATAG_DTB_COMPAT_CMDLINE_": {
							"FROM_BOOTLOADER": nil,
							"EXTEND":          nil, // mentioned in arch/arm/boot/compressed/
						},
					},
				},
			},
		},
	}

	// USE_OF        is used on all CPUs, *except* ARCH_FOOTBRIDGE, ARCH_RPC, and ARCH_SA1100
	// AUTO_ZRELADDR is used on all CPUs, *except* ARCH_FOOTBRIDGE, ARCH_RPC, and ARCH_SA1100
	//
	// ARM_APPENDED_DTB depends on OF
	// ZBOOT_ROM and ARM_APPENDED_DTB are mutually exclusive
	// ZBOOT_ROM and AUTO_ZRELADDR are mutally exclusive
	//
	// Therefore, ZBOOT_ROM is a V4-FOOTBDIRGE/RPC/SA1100 thing, where AUTO_ZRELADDR isn't forced on.
	v4FootbridgeRPCSA1100Syms = KconfigSyms{
		"CONFIG_AUTO_ZRELADDR": { // mentioned in arch/arm/boot/compressed/
			"": {
				"CONFIG_ZBOOT_ROM": { // mentioned in arch/arm/boot/compressed/
					"": {
						"CONFIG_USE_OF": { // mentioned in arch/arm/boot/compressed/
							"":  nil,
							"y": appendedDTBSyms, // mentioned in arch/arm/boot/compressed/, used to set UNCOMPRESS_INCLUDE in arch/arm/Kconfig.debug
						},
					},
					"y": {
						"CONFIG_USE_OF":         boolVal,             // mentioned in arch/arm/boot/compressed/
						"CONFIG_ZBOOT_ROM_BSS":  {"0xc1000000": nil}, // mentioned in arch/arm/boot/compressed/
						"CONFIG_ZBOOT_ROM_TEXT": {"0x80000": nil},    // mentioned in arch/arm/boot/compressed/
					},
				},
			},
			"y": {
				"CONFIG_USE_OF": { // mentioned in arch/arm/boot/compressed/
					"": nil,
					"y": {
						// EFI depends on OF && AUTO_ZRELADDR
						"CONFIG_EFI": boolVal, // CONFIG_EFI_STUB is mentioned in arch/arm/boot/compressed/
					},
				},
			},
		},
	}

	v45AlmostCommonSyms = Merge(appendedDTBSyms, KconfigSyms{ // common, except for V4-FOOTBRIDGE/RPC/SA1100
		// EFI depends on OF && AUTO_ZRELADDR && MMU && !CPU_BIG_ENDIAN
		"CONFIG_EFI": boolVal, // CONFIG_EFI_STUB is mentioned in arch/arm/boot/compressed/
	})

	v67CommonSyms = Merge(appendedDTBSyms, KconfigSyms{
		"CONFIG_CPU_": {
			"LITTLE_ENDIAN": {
				// EFI depends on OF && AUTO_ZRELADDR && MMU && !CPU_BIG_ENDIAN
				"CONFIG_EFI": boolVal, // CONFIG_EFI_STUB is mentioned in arch/arm/boot/compressed/
			},
			"BIG_ENDIAN": {
				"CONFIG_CPU_ENDIAN_": {
					// V6+ is BE8-only, not BE32 (which was a V5 thing)
					"BE8":  nil, // mentioned in arch/arm/boot/compressed/
				},
			},
		},
	})
)

var (
	// TODO: Figure out how to do non-MMU builds
	Matrix = KconfigSyms{
		"CONFIG_ARCH_MULTI_": {
			"V4": {
				"CONFIG_ARCH_": {
					"": v45AlmostCommonSyms,
					//"RPC": v4FootbridgeRPCSA1100Syms, // requires GCC < 9.1; mentioned in arch/arm/boot/compressed/, used to set UNCOMPRESS_INCLUDE in arch/arm/Kconfig.debug
					"SA1100": Merge(v4FootbridgeRPCSA1100Syms, KconfigSyms{ // mentioned in arch/arm/boot/compressed/, used to set textofs-y in arch/arm/Makefile

						// "collie" refers to the Sharp Zaurus SL-5500 PDA... I can't imagine
						// that it's physically possible to add a SA-1111 to that.  The only way
						// Kconfig allows turning SA1111 on is to either set SA1100_JORNADA720
						// (an HP-brand PDA) or ASSABET_NEPONSET (an Intel dev board) (build.go
						// knows to use the appropriate defconfig for this).  We could probably
						// try turning on both SA1100_COLLIE and JORNADA720 at the same time,
						// but I don't see a point in configuring a kernel that's schizophrenic
						// about which PDA it is.
						"CONFIG_SA1100_COLLIE": {
							"y": nil, // mentioned in arch/arm/boot/compressed/
							"": {
								"CONFIG_SA1111": boolVal, // used to set textofs-y in arch/arm/Makefile
							},
						},
					}),
				},
			},
			"V4T": Merge(v45AlmostCommonSyms, KconfigSyms{ // V4-with-thumb
				"CONFIG_ARCH_": {
					"":       nil,
					"EP93XX": nil, // mentioned in arch/arm/boot/compressed/
					"OMAP15XX": {
						"CONFIG_ATAGS":                   {"y": nil},
						"CONFIG_ARCH_OMAP1":              {"y": nil},
						"CONFIG_CPU_DCACHE_WRITETHROUGH": boolVal, // mentioned in arch/arm/boot/compressed/
					},
				},
			}),
			"V5": {
				"CONFIG_ARCH_": {
					"": Merge(v45AlmostCommonSyms, KconfigSyms{
						"CONFIG_CPU_FEROCEON_OLD_ID": boolVal,   // mentioned in arch/arm/boot/compressed/
						"CONFIG_CPU_XSCALE":          {"": nil}, // mentioned in arch/arm/boot/compressed/
						"CONFIG_CPU_":                {"LITTLE_ENDIAN": nil},
					}),
					"PXA": Merge(v45AlmostCommonSyms, KconfigSyms{
						"CONFIG_CPU_FEROCEON_OLD_ID": {"": nil},  // mentioned in arch/arm/boot/compressed/
						"CONFIG_CPU_XSCALE":          {"y": nil}, // mentioned in arch/arm/boot/compressed/
						"CONFIG_CPU_":                {"LITTLE_ENDIAN": nil},
						"CONFIG_PXA_SHARPSL": { // mentioned in arch/arm/boot/compressed/
							"": nil,
							"y": {
								"CONFIG_PXA_SHARPSL_DETECT_MACH_ID": boolVal, // mentioned in arch/arm/boot/compressed/
							},
						},
					}),
					"IXP4XX": { // IXP4XX forces ARM_APPENDEND_DTB=y, and big-endian means no EFI
						"CONFIG_CPU_FEROCEON_OLD_ID": {"": nil},  // mentioned in arch/arm/boot/compressed/
						"CONFIG_CPU_XSCALE":          {"y": nil}, // mentioned in arch/arm/boot/compressed/
						"CONFIG_CPU_":                {"BIG_ENDIAN": nil},
						"CONFIG_CPU_ENDIAN_": {
							// V5 is *only* BE32, BE8 won't come until V6
							"BE32": nil, // mentioned in arch/arm/boot/compressed/
						},
					},
				},
			},
			// All mentiones of CONFIG_CPU_V{6,6K,7} in arch/arm/boot/compressed/ are all
			//
			//	#if defined(CONFIG_CPU_V6) || defined(CONFIG_CPU_V6K) || defined(CONFIG_CPU_V7)
			//
			// so don't bother distinguishing between them.
			"V7": Merge(v67CommonSyms, KconfigSyms{
				"CONFIG_ARCH_": {
					"":        nil,
					"IPQ40XX": nil, // mach-qcom, used to set textofs-y in arch/arm/Makefile
					"MSM8960": nil, // mach-qcom, used to set textofs-y in arch/arm/Makefile
					"MSM8X60": nil, // mach-qcom, used to set textofs-y in arch/arm/Makefile
					"AXXIA":   nil, // mach-axxia, used to set textofs-y in arch/arm/Makefile
					"MESON":   nil, // mach-meson, used to set textofs-y in arch/arm/Makefile
					"REALTEK": nil, // mach-realtek, used to set textofs-y in arch/arm/Makefile
				},
				"CONFIG_THUMB2_KERNEL": boolVal, // mentioned in arch/arm/boot/compressed/
			}),
			// V7M is an MMU-les variant of V7.  We don't do non-MMU builds (yet?).
		},
		// arch/arm/Kconfig
		//"CONFIG_ARCH_ACORN": nil, // leave as default based on CONFIG_ARCH_RPC, mentioned in arch/arm/boot/compressed/

		// arch/arm/mm/Kconfig
		//"CONFIG_CPU_CP15":     nil, // leave as default based on the above, mentioned in arch/arm/boot/compressed/
		//"CONFIG_ARM_VIRT_EXT": nil, // leave as default based on CONFIG_CPU_==V7, mentioned in arch/arm/boot/compressed/

		// arch/arm/Kconfig-nommu
		//"CONFIG_PROCESSOR_ID": nil, // leave as default based on the above, mentioned in arch/arm/boot/compressed/

		// lib/Kconfig.kasan
		//"CONFIG_KASAN": boolVal, // only exists to disable it, mentioned in arch/arm/boot/compressed/

		// init/Kconfig
		"CONFIG_KERNEL_": {
			"GZIP": nil, // mentioned in arch/arm/boot/compressed/
			"LZ4":  nil, // mentioned in arch/arm/boot/compressed/
			"LZMA": nil, // mentioned in arch/arm/boot/compressed/
			"LZO":  nil, // mentioned in arch/arm/boot/compressed/
			"XZ":   nil, // mentioned in arch/arm/boot/compressed/
		},
		//"CONFIG_LD_ORPHAN_WARN":             boolVal, // mentioned in arch/arm/boot/compressed/
		//"CONFIG_LD_ORPHAN_WARN_LEVEL": nil, // leave as default, mentioned in arch/arm/boot/compressed/
	}
	DebugMatrix = KconfigSyms{
		"CONFIG_ARCH_MULTI_": {
			"V4": {
				"CONFIG_ARCH_": {
					"FOOTBRIDGE": v4FootbridgeRPCSA1100Syms, // used to set UNCOMPRESS_INCLUDE in arch/arm/Kconfig.debug
				},
			},
		},

		// kernel/trace/Kconfig
		"CONFIG_FUNCTION_TRACER": boolVal, // mentioned in arch/arm/boot/compressed/

		// arch/arm/Kconfig.debug
		"CONFIG_DEBUG_ICEDCC":            boolVal, // mentioned in arch/arm/boot/compressed/
		"CONFIG_DEBUG_SEMIHOSTING":       boolVal, // mentioned in arch/arm/boot/compressed/
		"CONFIG_DEBUG_UART_FLOW_CONTROL": boolVal, // mentioned in arch/arm/boot/compressed/
		"CONFIG_DEBUG_UNCOMPRESS":        boolVal, // mentioned in arch/arm/boot/compressed/
		"CONFIG_DEBUG_LL_INCLUDE": { // mentioned in arch/arm/boot/compressed/
			"":                     nil,
			"debug/8250.S":         nil,
			"debug/asm9260.S":      nil,
			"debug/at91.S":         nil,
			"debug/bcm63xx.S":      nil,
			"debug/brcmstb.S":      nil,
			"debug/clps711x.S":     nil,
			"debug/dc21285.S":      nil,
			"debug/digicolor.S":    nil,
			"debug/exynos.S":       nil,
			"debug/icedcc.S":       nil,
			"debug/imx.S":          nil,
			"debug/meson.S":        nil,
			"debug/msm.S":          nil,
			"debug/omap2plus.S":    nil,
			"debug/palmchip.S":     nil,
			"debug/pl01x.S":        nil,
			"debug/renesas-scif.S": nil,
			"debug/s3c24xx.S":      nil,
			"debug/s5pv210.S":      nil,
			"debug/sa1100.S":       nil,
			"debug/sti.S":          nil,
			"debug/stm32.S":        nil,
			"debug/tegra.S":        nil,
			"debug/ux500.S":        nil,
			"debug/vexpress.S":     nil,
			"debug/vf.S":           nil,
			"debug/vt8500.S":       nil,
			"debug/zynq.S":         nil,
			"mach/debug-macro.S":   nil,
		},
		//"CONFIG_UNCOMPRESS_INCLUDE": nil, // leave as default based on the above, mentioned in arch/arm/boot/compressed/

		// lib/Kconfig.debug
		//"CONFIG_FRAME_WARN":           {"0": nil, "1024": nil, "2048": nil}, // mentioned in arch/arm/boot/compressed/
	}
)
