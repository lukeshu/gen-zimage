// Copyright (C) 2024  Umorpha Systems
// SPDX-License-Identifier: AGPL-3.0-or-later

package matrix

import (
	"testing"
	"encoding/json"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMerge(t *testing.T) {
	type testcase struct {
		In  []KconfigSyms
		Out KconfigSyms
	}
	testcases := map[string]testcase{
		"disjoint": {
			In: []KconfigSyms{
				// 1
				{
					"CONFIG_CPU_": {
						"LITTLE_ENDIAN": nil,
						"BIG_ENDIAN": {
							"CONFIG_CPU_ENDIAN_": {
								"BE8":  nil,
								"BE32": nil,
							},
						},
					},
				},
				// 2
				{
					"CONFIG_ARCH_": {
						"":       nil,
						"EP93XX": nil,
					},
				},
			},
			Out: KconfigSyms{
				"CONFIG_CPU_": {
					"LITTLE_ENDIAN": nil,
					"BIG_ENDIAN": {
						"CONFIG_CPU_ENDIAN_": {
							"BE8":  nil,
							"BE32": nil,
						},
					},
				},
				"CONFIG_ARCH_": {
					"":       nil,
					"EP93XX": nil,
				},
			},
		},
		"nested-overlap":{ 
			In: []KconfigSyms{
				// 1
				{
					"CONFIG_ARCH_MULTI_": {
						"V4": {
							"CONFIG_ARCH_": {
								"": almostCommonSyms,
								"SA1100": {
									"CONFIG_SA1100_COLLIE": {
										"y": nil,
										"": {
											"CONFIG_SA1111": boolVal,
										},
									},
								},
							},
						},
					},
				},
				// 2
				{
					"CONFIG_ARCH_MULTI_": {
						"V4": {
							"CONFIG_ARCH_": {
								"FOOTBRIDGE": nil,
							},
						},
					},
				},
			},
			Out:  KconfigSyms{
				"CONFIG_ARCH_MULTI_": {
					"V4": {
						"CONFIG_ARCH_": {
							"": almostCommonSyms,
							"SA1100": {
								"CONFIG_SA1100_COLLIE": {
									"y": nil,
									"": {
										"CONFIG_SA1111": boolVal,
									},
								},
							},
							"FOOTBRIDGE": nil,
						},
					},
				},
			},
		},
	}
	t.Parallel()
	for tcName, tc := range testcases {
		tc := tc
		t.Run(tcName, func(t *testing.T) {
			t.Parallel()

			beforeJSON, err := json.Marshal(tc.In)
			require.NoError(t, err)
			var before []KconfigSyms
			require.NoError(t, json.Unmarshal(beforeJSON, &before))

			act := Merge(tc.In...)
			assert.Equal(t, tc.Out, act)

			// Check that Merge didn't mutate the inputs. 
			require.Equal(t, before, tc.In)
		})
	}
}
