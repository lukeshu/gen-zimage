// Copyright (C) 2024  Umorpha Systems
// SPDX-License-Identifier: AGPL-3.0-or-later

package build

import (
	_ "embed"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strings"

	"git.mothstuff.lol/lukeshu/gen-zimage/lib/objcopy"
)

func linuxRun(linuxGit string, args ...string) error {
	fmt.Fprintf(os.Stderr, "$ %s\n", strings.Join(args, " "))
	cmd := exec.Command(args[0], args[1:]...)
	cmd.Dir = linuxGit
	cmd.Stdout = os.Stderr
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func linuxOut(linuxGit string, args ...string) (string, error) {
	fmt.Fprintf(os.Stderr, "$ %s\n", strings.Join(args, " "))
	cmd := exec.Command(args[0], args[1:]...)
	cmd.Dir = linuxGit
	var stdout strings.Builder
	cmd.Stdout = &stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return "", err
	}
	ret := strings.TrimSpace(stdout.String())
	fmt.Fprintf(os.Stderr, "=> %s\n", ret)
	return ret, nil
}

var ancestorCache = map[string]bool{}

func isAncestor(linuxGit, ancestor, child string) bool {
	key := ancestor + ":" + child
	if ret, ok := ancestorCache[key]; ok {
		return ret
	}
	ancestorCache[key] = linuxRun(linuxGit, "git", "merge-base", "--is-ancestor", ancestor, child) == nil
	return ancestorCache[key]
}

type TooOldError struct {
	Ver string
	Msg string
}

func (e *TooOldError) Error() string {
	return fmt.Sprintf("kernel %s is too old: %s", e.Ver, e.Msg)
}

func PreFlight(linuxGit, commitish string, cfg map[string]string) error {
	if !isAncestor(linuxGit, "ef61ca88c511154d6bead23c08f9a021cfdfeb01", commitish) {
		return &TooOldError{Ver: commitish, Msg: "defconfig merging isn't added until 2.6.36"}
	}
	if cfg == nil {
		return nil
	}
	// Please sort these newest-version first.
	if cfg["CONFIG_ARCH_AXXIA"] == "y" && cfg["CONFIG_CPU_BIG_ENDIAN"] == "y" && !isAncestor(linuxGit, "5d6f52671e76ca2d55d74e676ac4c38ceb14a2d3", commitish) {
		return &TooOldError{Ver: commitish, Msg: "mach-axxia can't be built big-endian until 5.19"}
	}
	if cfg["CONFIG_ARCH_REALTEK"] == "y" && !isAncestor(linuxGit, "86aeee4d0a4cc5f7a28fe209444887b93a9a47ca", commitish) {
		return &TooOldError{Ver: commitish, Msg: "Realtek isn't added until 5.8"}
	}
	if cfg["CONFIG_ARCH_IPQ40XX"] == "y" && !isAncestor(linuxGit, "f125e2d4339dda6937865f975470b29c84714c9b", commitish) {
		return &TooOldError{Ver: commitish, Msg: "IPQ40XX isn't added until 5.7"}
	}
	if cfg["CONFIG_ARCH_MULTI_V4"] == "y" && cfg["CONFIG_ARCH_RPC"] != "y" && cfg["CONFIG_ARCH_SA1100"] != "y" && !isAncestor(linuxGit, "17723fd357f9973d5dd2908e3cc6b4149d891429", commitish) {
		return &TooOldError{Ver: commitish, Msg: "a good ARMv4 non-RiscPC non-SA-1100 defconfig (moxart_defconfig) isn't added until 3.14"}
	}
	if cfg["CONFIG_KERNEL_LZ4"] == "y" && !isAncestor(linuxGit, "f9b493ac9b833fd9dd3bbd50460adb33f29e1238", commitish) {
		return &TooOldError{Ver: commitish, Msg: "LZ4 isn't added added until 3.11"}
	}
	if cfg["CONFIG_KERNEL_XZ"] == "y" && !isAncestor(linuxGit, "a7f464f3db93ae5492bee6f6e48939fd8a45fa99", commitish) {
		return &TooOldError{Ver: commitish, Msg: "XZ isn't added until 3.4"}
	}
	return nil
}

//go:embed buildsys-2.6.12.patch
var patchContent_2_6_12 string

//go:embed buildsys-2.6.17.patch
var patchContent_2_6_17 string

//go:embed buildsys-3.7.patch
var patchContent_3_7 string

//go:embed buildsys-4.8.patch
var patchContent_4_8 string

//go:embed buildsys-4.20.patch
var patchContent_4_20 string

//go:embed buildsys-5.4.patch
var patchContent_5_4 string

//go:embed buildsys-5.6.patch
var patchContent_5_6 string

//go:embed buildsys-6.1.patch
var patchContent_6_1 string

func BuildZImage(linuxGit, commitish string, cfg map[string]string, imgFilename, outFilename string) error {
	if err := checkout(linuxGit, commitish); err != nil {
		return err
	}
	if err := configure(linuxGit, cfg); err != nil {
		return err
	}
	if err := build(linuxGit, imgFilename, outFilename); err != nil {
		return err
	}
	return nil
}

func checkout(linuxGit, commitish string) error {
	have, err := linuxOut(linuxGit, "git", "rev-parse", "HEAD^{tree}")
	if err != nil {
		return err
	}
	want, err := linuxOut(linuxGit, "git", "rev-parse", commitish+"^{tree}")
	if err != nil {
		return err
	}
	if have == want {
		return nil
	}

	if err := linuxRun(linuxGit, "git", "reset", "--hard"); err != nil {
		return err
	}
	if err := linuxRun(linuxGit, "git", "clean", "-fdx"); err != nil {
		return err
	}
	clear(ancestorCache)
	if err := linuxRun(linuxGit, "git", "checkout", commitish); err != nil {
		return err
	}

	if isAncestor(linuxGit, "059bc9fc375e00f159f9d7c5ca9d18ab843f95de", commitish) && !isAncestor(linuxGit, "e33a814e772cdc36436c8c188d8c42d019fda639", commitish) {
		if err := linuxRun(linuxGit, "git", "cherry-pick", "-n", "e33a814e772cdc36436c8c188d8c42d019fda639"); err != nil {
			return err
		}
	}

	var patchContent string
	switch {
	case isAncestor(linuxGit, "92481c7d14b8030418f00c4b4ec65556565d892d", commitish):
		patchContent = patchContent_6_1
	case isAncestor(linuxGit, "46b7c49254f89d54f11c58fa629f66e224a16034", commitish):
		patchContent = patchContent_5_6
	case isAncestor(linuxGit, "2042b5486bd311db67b85915ee6291905b72e270", commitish):
		patchContent = patchContent_5_4
	case isAncestor(linuxGit, "25815cf5ffecfb8b54314ee1b7cf14c78681fbb6", commitish):
		patchContent = patchContent_4_20
	case isAncestor(linuxGit, "c6bbfbb729e88188b7c9113d58647766db03f1d4", commitish):
		patchContent = patchContent_4_8
	case isAncestor(linuxGit, "2d4d07b97c0b774ea9ce2a2105818208d3df7241", commitish):
		patchContent = patchContent_3_7
	case isAncestor(linuxGit, "4f1933620f57145212cdbb1ac6ce099eeeb21c5a", commitish):
		patchContent = patchContent_2_6_17
	default:
		patchContent = patchContent_2_6_12
	}

	cmd := exec.Command("patch", "-p1")
	cmd.Stdin = strings.NewReader(patchContent)
	cmd.Dir = linuxGit
	cmd.Stdout = os.Stderr
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}

func configure(linuxGit string, cfg map[string]string) error {
	if cfg["CONFIG_ARCH_EP93XX"] == "y" && (cfg["CONFIG_EFI"] == "y" || cfg["CONFIG_ARM_APPENDED_DTB"] == "y") && !isAncestor(linuxGit, "0361c7e504b1fa3c2901643088e2f29c0354ab31", "HEAD") {
		// Prior to v5.19 ep93xx didn't support multiplatform,
		// and so USE_OF (which is required for EFI or
		// appended DTB) defaulted to off.
		cfg["CONFIG_USE_OF"] = "y"
	}
	if cfg["CONFIG_ARCH_PXA"] == "y" && (cfg["CONFIG_EFI"] == "y" || cfg["CONFIG_ARM_APPENDED_DTB"] == "y") && !isAncestor(linuxGit, "250c1a694ff304e5d69e74ab32755eddcc2b8f65", "HEAD") {
		// Prior to v5.19 pxa didn't support multiplatform,
		// and so USE_OF (which is required for EFI or
		// appended DTB) defaulted to off.
		cfg["CONFIG_USE_OF"] = "y"
	}
	if cfg["CONFIG_CPU_LITTLE_ENDIAN"] == "y" && !isAncestor(linuxGit, "0361c7e504b1fa3c2901643088e2f29c0354ab31", "HEAD") {
		// Prior to v5.19, there was no separate
		// CPU_LITTLE_ENDIAN, it was just leaving
		// CONFIG_CPU_BIG_ENDIAN unset.
		delete(cfg, "CONFIG_CPU_LITTLE_ENDIAN")
	}

	_ = os.Mkdir(filepath.Join(linuxGit, "kernel/configs"), 0777)
	cfgFile, err := os.OpenFile(filepath.Join(linuxGit, "kernel/configs/gen-zimage.config"), os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	keys := make([]string, 0, len(cfg))
	for key := range cfg {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, key := range keys {
		val := cfg[key]
		var err error
		if val == "" {
			_, err = fmt.Fprintf(cfgFile, "# %s is not set\n", key)
		} else {
			_, err = fmt.Fprintf(cfgFile, "%s=%s\n", key, val)
		}
		if err != nil {
			return err
		}
	}
	if err := cfgFile.Close(); err != nil {
		return err
	}

	defconfig := "defconfig"
	switch {
	case cfg["CONFIG_ARCH_MULTI_V4"] == "y":
		switch {
		case cfg["CONFIG_ARCH_RPC"] == "y":
			defconfig = "rpc_defconfig"
		case cfg["CONFIG_ARCH_SA1100"] == "y":
			switch {
			case cfg["CONFIG_SA1100_COLLIE"] == "y":
				// "collie" refers to the Zaurus SL-5500 PDA made by Sharp.
				defconfig = "collie_defconfig"
			case cfg["CONFIG_SA1111"] == "y":
				// Neponset is the SA-1111 add-on board to the Assabet.
				// Jornada 720 is a PDA made by HP.
				defconfig = "neponset_defconfig" // or jornada720_defconfig
			default:
				// Assabet is Intel's SA-1100 development board.
				// The iPAQ H3600 is a PDA made by Compaq.
				defconfig = "assabet_defconfig" // or h3600_defconfig
			}
		default:
			defconfig = "moxart_defconfig"
		}
	case cfg["CONFIG_ARCH_MULTI_V4T"] == "y":
		switch {
		case cfg["CONFIG_ARCH_EP93XX"] == "y":
			defconfig = "ep93xx_defconfig"
		default:
			defconfig = "multi_v4t_defconfig"
		}
	case cfg["CONFIG_ARCH_MULTI_V5"] == "y":
		switch {
		case cfg["CONFIG_ARCH_PXA"] == "y":
			defconfig = "pxa_defconfig"
		case cfg["CONFIG_ARCH_IXP4XX"] == "y":
			defconfig = "ixp4xx_defconfig"
		case cfg["CONFIG_CPU_FEROCEON_OLD_ID"] == "y":
			defconfig = "mvebu_v5_defconfig" // or mv78xx0_defconfig or orion5x_defconfig
		default:
			defconfig = "multi_v5_defconfig"
		}
	case cfg["CONFIG_ARCH_MULTI_V7"] == "y":
		switch {
		case cfg["CONFIG_ARCH_AXXIA"] == "y":
			defconfig = "axm55xx_defconfig"
		//case cfg["CONFIG_ARCH_REALTEK"] == "y":
		//	defconfig = "???"
		default:
			defconfig = "multi_v7_defconfig"
		}
	}

	if err := linuxRun(linuxGit, "make", "ARCH=arm", "CROSS_COMPILE=arm-linux-gnueabi-", defconfig); err != nil {
		return err
	}

	if isAncestor(linuxGit, "63a91033d52e64a22e571fe84924c0b7f21c280d", "HEAD") {
		// v4.0 and later is easy; we have a make target
		if err := linuxRun(linuxGit, "make", "ARCH=arm", "CROSS_COMPILE=arm-linux-gnueabi-", "gen-zimage.config"); err != nil {
			return err
		}
	} else {
		// ... but for older versions we have to run `merge_config.sh && make olddefconfig` ourselves
		if !isAncestor(linuxGit, "4b5f72145e3ba85e38240dba844ebe1fcbb73713", "HEAD") {
			// ...and for real old versions we might even need to back-port the merge_config.sh script!
			if err := linuxRun(linuxGit, "git", "checkout", "v3.3", "scripts/kconfig/merge_config.sh"); err != nil {
				return err
			}
		}

		// Run `merge_config.sh`.
		cmd := []string{filepath.Join(linuxGit, "scripts/kconfig/merge_config.sh"), "-m", ".config", "kernel/configs/gen-zimage.config"}
		if !isAncestor(linuxGit, "55cae3043a48e01f8fc31e8aecc3062c4767a27d", "HEAD") {
			// Early versions (pre-v3.4) don't have the execute bit set, so we need to use `sh` explicitly.
			cmd = append([]string{"sh"}, cmd...)
		}
		if err := linuxRun(linuxGit, cmd...); err != nil {
			return err
		}

		// Run `make olddefconfig`.
		var olddefconfig string
		switch {
		case isAncestor(linuxGit, "fb16d8912db5268f29706010ecafff74b971c58d", "HEAD"):
			// v3.7 and later
			olddefconfig = "olddefconfig"
		case isAncestor(linuxGit, "ef61ca88c511154d6bead23c08f9a021cfdfeb01", "HEAD"):
			// v2.6.36 (2010) and later
			olddefconfig = "oldnoconfig"
		default:
			panic("should not happen -- PreFlight() should have prevented this")
		}
		if err := linuxRun(linuxGit, "make", "ARCH=arm", olddefconfig); err != nil {
			return err
		}
	}

	// sanity check
	bs, err := os.ReadFile(filepath.Join(linuxGit, ".config"))
	if err != nil {
		return err
	}
	actCfg := make(map[string]string)
	for _, line := range strings.Split(string(bs), "\n") {
		if m := reConfigSet.FindStringSubmatch(line); m != nil {
			actCfg[m[1]] = m[2]
		} else if m := reConfigNotSet.FindStringSubmatch(line); m != nil {
			actCfg[m[1]] = ""
		}
	}
	badCfg := false
	for k, expV := range cfg {
		actV, _ := actCfg[k]
		if actV != expV {
			if k == "CONFIG_ARCH_MULTI_V4" && expV == "y" && (actCfg["CONFIG_ARCH_SA1100"] == "y" || actCfg["CONFIG_ARCH_RPC"] == "y") {
				continue
			}
			if k == "CONFIG_ARCH_MULTI_V4T" && expV == "y" && (actCfg["CONFIG_ARCH_EP93XX"] == "y" || actCfg["CONFIG_ARCH_OMAP1"] == "y") {
				continue
			}
			if k == "CONFIG_ARCH_MULTI_V5" && expV == "y" && (actCfg["CONFIG_ARCH_IXP4XX"] == "y" || actCfg["CONFIG_ARCH_PXA"] == "y") {
				continue
			}
			fmt.Fprintf(os.Stderr, ""+
				"Value requested for %q not in final .config:\n"+
				"\tRequested value: %q\n"+
				"\tActual value:    %q\n",
				k, expV, actV)
			badCfg = true
		}
	}
	if badCfg {
		return fmt.Errorf("could not generate desired config")
	}

	return nil
}

var (
	reConfigSet    = regexp.MustCompile(`^(CONFIG_[a-zA-Z0-9_]+)=(.*)`)
	reConfigNotSet = regexp.MustCompile(`^# (CONFIG_[a-zA-Z0-9_]+) is not set$`)
)

func build(linuxGit, imgFilename, outFilename string) error {
	if err := linuxRun(linuxGit, "make", "ARCH=arm", "CROSS_COMPILE=arm-linux-gnueabi-", "image="+imgFilename, "arm-zimage"); err != nil {
		return err
	}

	// sanity check
	if err := objcopy.ObjCopy(filepath.Join(linuxGit, "arch/arm/boot/compressed/vmlinux"), outFilename+".full.bin", false, nil); err != nil {
		return err
	}
	if exec.Command("cmp", "-s", filepath.Join(linuxGit, "arch/arm/boot/zImage"), outFilename+".full.bin").Run() != nil {
		return fmt.Errorf("our objcopy disagrees with their objcopy: %q %q", filepath.Join(linuxGit, "arch/arm/boot/zImage"), outFilename+".full")
	}
	if err := os.Remove(outFilename + ".full.bin"); err != nil {
		return err
	}

	// Write {outputFilename}.layout and {outputFilename}.bin
	log, err := os.OpenFile(outFilename+".layout", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	if err := objcopy.ObjCopy(filepath.Join(linuxGit, "arch/arm/boot/compressed/vmlinux"), outFilename+".bin", true, log); err != nil {
		_ = log.Close()
		return err
	}
	if err := log.Close(); err != nil {
		return err
	}

	// Write {outputFilename}.config
	cfg, err := os.ReadFile(filepath.Join(linuxGit, ".config"))
	if err != nil {
		return err
	}
	if err := os.WriteFile(outFilename+".config", cfg, 0666); err != nil {
		return err
	}

	return nil
}
