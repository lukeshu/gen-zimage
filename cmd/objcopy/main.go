// Copyright (C) 2024  Umorpha Systems
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"flag"
	"fmt"
	"os"

	"git.mothstuff.lol/lukeshu/gen-zimage/lib/objcopy"
)

func main() {
	if err := mainWithErr(); err != nil {
		fmt.Fprintf(os.Stderr, "%s: error: %v\n", os.Args[0], err)
		os.Exit(1)
	}
}

func mainWithErr() error {
	var scramble bool
	flag.BoolVar(&scramble, "scramble", false, "overwrite machine code with garbage")
	flag.Parse()
	if flag.NArg() != 2 {
		return fmt.Errorf("expected 2 positional args")
	}
	return objcopy.ObjCopy(flag.Args()[0], flag.Args()[1], scramble, os.Stderr)
}
