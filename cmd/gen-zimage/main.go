// Copyright (C) 2024  Umorpha Systems
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	_ "embed"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"git.mothstuff.lol/lukeshu/gen-zimage/lib/build"
	"git.mothstuff.lol/lukeshu/gen-zimage/lib/matrix"
)

const (
	flagBuild                = true
	flagKeepGoingThisVersion = false
	flagKeepGoingNextVersion = false
)

func main() {
	if err := mainWithErr(os.Args[1]); err != nil {
		fmt.Fprintf(os.Stderr, "%s: error: %v\n", os.Args[0], err)
		os.Exit(1)
	}
}

func reverse[T any](ary []T) {
	for i := 0; i < len(ary)/2; i++ {
		ary[i], ary[len(ary)-1-i] = ary[len(ary)-1-i], ary[i]
	}
}

//go:embed image.txt
var imgBytes []byte

func mainWithErr(linuxGit string) error {
	var stdout strings.Builder
	cmd := exec.Command("git", "tag")
	cmd.Dir = linuxGit
	cmd.Stdout = &stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	var versions []string
	for _, tag := range strings.Split(stdout.String(), "\n") {
		if !strings.HasPrefix(tag, "v") || strings.Contains(tag, "-") {
			continue
		}
		versions = append(versions, tag)
	}
	reverse(versions)

	tmpdir, err := os.MkdirTemp("", "gen-zimage.*")
	if err != nil {
		return err
	}

	imgFilename := filepath.Join(tmpdir, "image")
	if err := os.WriteFile(imgFilename, imgBytes, 0666); err != nil {
		return err
	}

	i := 0
	seen := make(map[string]bool)
	var errs []error
	for _, ver := range versions {
		if err := build.PreFlight(linuxGit, ver, nil); err != nil {
			var tooOldErr *build.TooOldError
			if errors.As(err, &tooOldErr) {
				fmt.Fprintf(os.Stderr, "-> skipping version: %v\n", err)
				continue
			}
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			errs = append(errs, err)
			break
		}
		if err := os.MkdirAll(filepath.Join("out", "linux"+strings.TrimPrefix(ver, "v")), 0777); err != nil {
			return err
		}
		verFailed := false
		if err := matrix.Matrix.Iter(make(map[string]string), func(cfg map[string]string) error {
			name := nameBuild(ver, cfg)
			if err := build.PreFlight(linuxGit, ver, cfg); err != nil {
				err = fmt.Errorf("%s: %w", name, err)
				var tooOldErr *build.TooOldError
				if errors.As(err, &tooOldErr) {
					fmt.Fprintf(os.Stderr, "-> skipping build: %v\n", err)
					return nil
				}
				verFailed = true
				return err
			}

			i++
			if seen[name] {
				return fmt.Errorf("dup: %q", name)
			}
			seen[name] = true
			fmt.Fprintf(os.Stderr, "%d: %s\n", i, name)
			filename := filepath.Join("out", "linux"+strings.TrimPrefix(ver, "v"), name)
			if _, err := os.Stat(filename + ".bin"); err != nil && os.IsNotExist(err) {
				if flagBuild {
					if err := build.BuildZImage(linuxGit, ver, cfg, imgFilename, filename); err != nil {
						err = fmt.Errorf("%s: %w", name, err)
						verFailed = true
						if flagKeepGoingThisVersion {
							fmt.Fprintf(os.Stderr, "error: %v\n", err)
							errs = append(errs, err)
						} else {
							return err
						}
					}
				} else {
					fmt.Fprintf(os.Stderr, "-> skipping build\n")
				}
			} else {
				fmt.Fprintf(os.Stderr, "-> already built\n")
			}
			return nil
		}); err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			errs = append(errs, err)
		}
		if verFailed && !flagKeepGoingNextVersion {
			break
		}
	}

	for i, err := range errs {
		fmt.Fprintf(os.Stderr, "error %d/%d: %v\n", i+1, len(errs), err)
	}
	fmt.Printf("i : %d\n", i)

	if err := os.RemoveAll(tmpdir); err != nil {
		return err
	}

	if len(errs) > 0 {
		return fmt.Errorf("encountered %d errors", len(errs))
	}
	return nil
}

func nameBuild(ver string, cfg map[string]string) string {
	// Kernel
	name := "linux" + strings.TrimPrefix(ver, "v")

	// CPU architecture
	armv := strings.ToLower(getChoice(cfg, "CONFIG_ARCH_MULTI_", "V4", "V4T", "V5", "V6", "V7"))
	name += "-arm" + armv
	endian := strings.ToLower(getChoice(cfg, "CONFIG_CPU_ENDIAN_", "BE8", "BE32"))
	if endian == "" {
		endian = "le"
	}
	name += "-" + endian
	subarch := strings.ToLower(getChoice(cfg, "CONFIG_ARCH_",
		// V4
		"RPC",
		"SA1100",
		// V4T
		"EP93XX",
		"OMAP15XX",
		// V5
		"PXA",
		"IXP4XX",
		// V6
		// V7
		"IPQ40XX",
		"MSM8960",
		"MSM8X60",
		"AXXIA",
		"MESON",
		"REALTEK",
	))
	if subarch == "" {
		subarch = "generic"
	}
	switch armv {
	case "v4":
		switch subarch {
		case "sa1100":
			if cfg["CONFIG_SA1100_COLLIE"] == "y" {
				subarch += "-collie"
			}
			if cfg["CONFIG_SA1111"] == "y" {
				subarch += "-sa1111"
			}
		}
		if subarch == "footbridge" || subarch == "rpc" || strings.HasPrefix(subarch, "sa1100") {
			// These are assumed to on all other subarches.
			if cfg["CONFIG_USE_OF"] == "y" {
				subarch += "-of"
			}
			if cfg["CONFIG_AUTO_ZRELADDR"] == "y" {
				subarch += "-autozreladdr"
			}
		}
	}
	name += "-" + subarch

	// CPU-specific flags.
	switch armv {
	case "v4t":
		if cfg["CONFIG_CPU_DCACHE_WRITETHROUGH"] == "y" {
			name += "-writethrough"
		}
	case "v5":
		if cfg["CONFIG_PXA_SHARPSL"] == "y" {
			name += "-sharpsl"
			if cfg["CONFIG_PXA_SHARPSL_DETECT_MACH_ID"] == "y" {
				name += "-autoid"
			}
		}
		if cfg["CONFIG_CPU_FEROCEON_OLD_ID"] == "y" {
			name += "-oldid"
		}
	case "v7":
		if cfg["CONFIG_THUMB2_KERNEL"] == "y" {
			name += "-thumb2"
		}
	}

	// Boot settings
	if cfg["CONFIG_ZBOOT_ROM"] == "y" {
		name += "-zbootrom"
	}
	if cfg["CONFIG_ARM_APPENDED_DTB"] == "y" {
		name += "-dtb"
		switch getChoice(cfg, "CONFIG_ARM_ATAG_DTB_COMPAT_CMDLINE_", "FROM_BOOTLOADER", "EXTEND") {
		case "FROM_BOOTLOADER":
			name += "replace"
		case "EXTEND":
			name += "extend"
		}
	}
	if cfg["CONFIG_EFI"] == "y" {
		name += "-efistub"
	}

	// Build settings
	name += "-" + strings.ToLower(getChoice(cfg, "CONFIG_KERNEL_", "GZIP", "LZ4", "LZMA", "LZO", "XZ"))

	return name
}

func getChoice(cfg map[string]string, prefix string, suffixes ...string) string {
	for _, suffix := range suffixes {
		if cfg[prefix+suffix] == "y" {
			return suffix
		}
	}
	return ""
}

func tern(x bool, a, b string) string {
	if x {
		return a
	}
	return b
}
